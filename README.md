SYNOPSIS
========

    # export Bin() and Script() by default.

    use FindBin;

    my $dir_from    = Bin();        # $dir_from is an IO object
    my $script_base = Script();     # $script_base is a string.

    # explicitly export only Bin() or Script().

    use FindBin :Bin;
    use FindBin :Script;

    # set verbose or resolve to True by default in the current
    # lexical scope.
    #
    # caller can examine constants
    #   MY::_FindBin-verbose-def
    #   MY::_FindBin-resolve-def
    # to determine the default.
    #
    # verbose output is sent to stderr via "note" with a 
    # leading '#'.

    use FindBin :verbose;
    use FindBin :resolve;
    use FindBin ( :verbose :resolve );

    # request the bin path with symlinks resolved
    # regardless of the default at use time.
    # Bin will have symlinks along the path resolved,
    # Script will return the basename of any resolved
    # symlink.

    my $resolved    = Bin( :resolve  );

    # determine if the current executable is running
    # from a symlinked path. stringy compare of the
    # IO objects does the right thing.

    my $dir         = Bin( :!resolve );
    my $abs         = Bin( :resolve  );

    $dir ne $abs
    and say "There is a symlink in path '$dir'";

    # make every subsequent call to Bin() default to
    # Bin( :resolve )
    # within the current lexical scope.

    use FindBin :resolve;

    # messages might be useful for dealing with
    # stray symlinks in filesystem.

    my $dir_from    = Bin( :verbose );

    # make every subsequent call to Bin() default to Bin(:verbose)
    # within the current lexical scope.

    use FindBin :verbose;

DESCRIPTION
===========

This module is used to locate the currently running Perl program and the diretory it is running from.

Command-line use of perl6 -, perl6 -e or interactive use of the REPL will cause `Script()` to return `"-"`, `"-e"`, or `"interactive"` and `Bin()` to return `$*CWD`.

Otherwise `Script()` will return the basename of the running program and `Bin()` will return the name of the directory where the program was run from (taken from `$*PROGRAM` when Bin is called).

Note that `Bin()` returns an IO object, so if you want the path as a string, use: `~Bin()` or `Bin.Str`.

`Bin()` takes optional named arguments:

Pod::Defn<94311307182528>

Causes `Bin()` to return the bin value after processing with `.resolve` to convert any symbolic links in the path to an absolute path.

Pod::Defn<94311310732912>

:verbose turns on verbose reporting (to STDERR) for debugging purposes, including the path used, resolve status, and returned value.

You can permanently change the defaults for these two named options within the current lexical scope, by passing the options to your `use FindBin` statement. For example:

    # make Bin() default to Bin(:resolve)
    use FindBin :Bin, :resolve;

    # alternatively, make Bin() default to Bin(:resolve, :verbose)
    use FindBin :Bin, :resolve, :verbose;

Note that explicitly passing a `:!resolve` or `:!verbose` to `Bin()` will override these defaults:

    use FindBin :verbose;

    my $dir = Bin( :!verbose );

Tests use symlinks
------------------

To see how symlinks are handled, do an ls -l on ./t and look at the tests symlinked to ./symlinks or ./bin.

SEE ALSO
========

Perl variables `$*PROGRAM`, `$*PROGRAM-NAME`:

    https://docs.perl6.org/language/variables#index-entry-%24%2APROGRAM

Class which implements the dirname, basename, and absolute methods use to determine the absolute path returned by `Bin()`.

    https://docs.perl6.org/type/IO::Path

AUTHOR
======

Steven Lembark <lembark@wrkhors.com>

COPYRIGHT AND LICENSE
=====================

Copyright 2018-2019 Steven Lembark

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0 or any later version.

