########################################################################
# module makes FindBin::Bin & ::Script available as late calls in
# in the code available.
########################################################################
########################################################################
# housekeeping
########################################################################

use v6.d;

unit module FindBin:ver<0.2.2>:auth<CPAN:lembark>;

# constants are exported into caller's space via use,
# supply context defaults for args in Bin & Script.

constant _FindBin_RESOLVE-DEF is export( :resolve ) = True;
constant _FindBin_VERBOSE-DEF is export( :verbose ) = True;

constant OPTION-TAGS    = |( :resolve, :verbose );

# decided once at use time, no reson to re-compute it:

constant IS-INTERACTIVE = $*PROGRAM-NAME eq '-e' | '-' | 'interactive';

################################################################
# exported (API)
################################################################

our sub Script 
(
    :$resolve is copy = DYNAMIC::_FindBin_RESOLVE-DEF,
    :$verbose is copy = DYNAMIC::_FindBin_VERBOSE-DEF
    --> Str
)
is export( :Script, :DEFAULT, OPTION-TAGS )
{
    if $verbose
    {
        note "# Resolve:     {?$resolve}";
        note "# Interactive: '{IS-INTERACTIVE}'";
        note "# Path:        '$*PROGRAM-NAME'";
    }

    IS-INTERACTIVE
    ?? ~$*PROGRAM-NAME
    !! $resolve
    ?? $*PROGRAM.resolve.basename
    !! $*PROGRAM.basename
}

our sub Bin
(
    :$resolve is copy = DYNAMIC::_FindBin_RESOLVE-DEF,
    :$verbose is copy = DYNAMIC::_FindBin_VERBOSE-DEF
    --> IO
)
is export( :Bin, :DEFAULT, OPTION-TAGS )
{
    # resolve returns an IO.
    # absolute returns a string.
    # dirname returns a string.

    my $bin_from
    =  IS-INTERACTIVE 
    ?? $*CWD 
    !! $*PROGRAM.IO
    ;

    my $path 
    = $resolve
    ?? $bin_from.resolve
    !! $bin_from.absolute.IO
    ;

    if $verbose
    {
        note "# Resolve:  {?$resolve}";
        note "# Bin from: '$bin_from'";
        note "# Path is:  '$path'";
    }

    # interactive based on CWD returns the directory
    # as-is, otherwise return the executable path's
    # directory.

    IS-INTERACTIVE
    ?? $path
    !! $path.dirname.IO
}

=finish
