# $ perl6 -M'FindBin' -e 'say Bin.Str';
# # Resolve:  True
# # Bin from: '-e'
# # Path is:  '/sandbox/lembark/Modules/Perl6/FindBin/-e'
# /sandbox/lembark/Modules/Perl6/FindBin
 
use v6.d;
use Test;

# make the module accessable to the perl6 command line code.

my $expect  = '-e';

my ( $found ) = qx{ perl6 -M'FindBin' -e 'say Script' }.chomp;

say "# Expect: '$expect'";
say "# Found:  '$found'";

is $found, $expect, "'perl6 -e' returns '$found' ($expect)";


done-testing;
