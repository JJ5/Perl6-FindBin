use v6.d;
use Test;

eval-lives-ok
Q
{
    use FindBin;

    ok MY::<&Bin>,      'Bin exists.';

    say '# Bin is:    ' ~ Bin;
},
'&Bin is callable';

ok ! MY::<&Bin>,        'Bin is lexicaly scoped.';

eval-lives-ok
Q
{
    use FindBin;

    ok MY::<&Script>,   'Script exists.';

    say '# Script is: ' ~ Script;
},
'&Script is callable';

do
{
    use FindBin :Bin;

    ok   MY::<&Bin>,      'Bin exists.';
    ok ! MY::<&Script>,   'Script does not.';
};

ok ! MY::<&Bin>,        'Bin is lexicaly scoped.';
ok ! MY::<&Script>,     'Script is lexicaly scoped.';

do
{
    use FindBin :Script;

    ok MY::<&Script>,   'Script exists.';

    ok   MY::<&Script>,   'Script exists.';
    ok ! MY::<&Bin>,      'Bin does not.';
},
'&Script is callable';

ok ! MY::<&Bin>,        'Bin is lexicaly scoped.';
ok ! MY::<&Script>,     'Script is lexicaly scoped.';

done-testing;
