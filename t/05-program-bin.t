use v6.d;
use Test;

use FindBin ( :verbose );

say 'Program is: ' ~ $*PROGRAM.absolute;

ok   MY::_FindBin_VERBOSE-DEF, 'true  :verbose';
ok ! MY::_FindBin_RESOLVE-DEF, 'false :resolve';

my $found   = Bin;
my $expect  = $*PROGRAM.absolute.IO.dirname.IO;

ok $found,  'Bin returns ' ~ $found; 
ok $expect, 'Dir is ' ~ $expect;
is $found, $expect, 'Bin matches dirname.';

done-testing;
