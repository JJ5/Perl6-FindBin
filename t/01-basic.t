use v6.d;
use Test;

my $madness = 'FindBin';

use-ok $madness;

lives-ok 
{
    require ::( $madness );

    my $found   = ::( $madness ).^name;

    is $found, $madness, 'Found package.';
},
"Package '$madness' available.";

done-testing;
