use v6.d;
use Test;

use FindBin;

my $found   = Script;
my $expect  = $*PROGRAM.basename;

ok $found,  'Script returns ' ~ $found; 
ok $expect, '$*PROGRAM.basename is ' ~ $expect;
is $found, $expect, 'Script matches basename.';

done-testing;
