use v6.d;
use Test;

# FindBin is usable from 01, 02 tests.
# verify that :verbose, :resolve args export the 
# constants into the appropriate scope.

ok ! MY::_FindBin_VERBOSE-DEF, 'false :verbose outside of block';
ok ! MY::_FindBin_RESOLVE-DEF, 'false :resolve outside of block';

do
{
    use FindBin;

    ok ! MY::_FindBin_VERBOSE-DEF, 'false :verbose inside of block';
    ok ! MY::_FindBin_RESOLVE-DEF, 'false :resolve inside of block';
};

ok ! MY::_FindBin_VERBOSE-DEF, 'false :verbose outside of block';
ok ! MY::_FindBin_RESOLVE-DEF, 'false :resolve outside of block';

do
{
    use FindBin  :verbose;

    ok   MY::_FindBin_VERBOSE-DEF, 'true  :verbose inside of block';
    ok ! MY::_FindBin_RESOLVE-DEF, 'false :resolve inside of block';
};

ok ! MY::_FindBin_VERBOSE-DEF, 'false :verbose outside of block';
ok ! MY::_FindBin_RESOLVE-DEF, 'false :resolve outside of block';

do
{
    use FindBin  :resolve;

    ok ! MY::_FindBin_VERBOSE-DEF, 'false :verbose inside of block';
    ok   MY::_FindBin_RESOLVE-DEF, 'true  :resolve inside of block';
};

ok ! MY::_FindBin_VERBOSE-DEF, 'false :verbose outside of block';
ok ! MY::_FindBin_RESOLVE-DEF, 'false :resolve outside of block';

do
{
    use FindBin ( :resolve :verbose );

    ok   MY::_FindBin_VERBOSE-DEF, 'true  :verbose inside of block';
    ok   MY::_FindBin_RESOLVE-DEF, 'true  :resolve inside of block';
};

ok ! MY::_FindBin_VERBOSE-DEF, 'false :verbose outside of block';
ok ! MY::_FindBin_RESOLVE-DEF, 'false :resolve outside of block';

do
{
    use FindBin ( :verbose :resolve );

    ok   MY::_FindBin_VERBOSE-DEF, 'true  :verbose inside of block';
    ok   MY::_FindBin_RESOLVE-DEF, 'true  :resolve inside of block';
};

ok ! MY::_FindBin_VERBOSE-DEF, 'false :verbose outside of block';
ok ! MY::_FindBin_RESOLVE-DEF, 'false :resolve outside of block';

# ok, at this point there doesn't seem to be any way of making
# the _FindBin_*-def constants leak out of their scope.

done-testing;
